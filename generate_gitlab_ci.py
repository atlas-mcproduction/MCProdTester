#!/usr/bin/env python

# %% Important packages
import argparse

import yaml

import importlib_resources

from pathlib import Path

from mcprodtester import workflow
from mcprodtester import tags
from mcprodtester import request

# %% Input arguments
parser = argparse.ArgumentParser(description='Generate a GitLab CI for all samples.')
parser.add_argument('request' , help='Request file')
parser.add_argument('--output','-o' ,default='requestci.yaml',help='Location of CI jobs YAML file.')

args = parser.parse_args()

# %% Open the request
request = request.load(args.request)

# %% Prepare base file from a template
ci = {}
with importlib_resources.files('mcprodtester.data').joinpath('dynamic.yml').open() as fh:
    ci = yaml.safe_load(fh)

# %% Loop over all samples
for i,sample in enumerate(request.get('samples',[])):
    name = f'{sample["dsid"]}_{sample["workflow"]}'
    ci[name]={
        'extends':'.run_test_sample',
        'variables': {
            'REQUEST':args.request,
            'SAMPLE':i
        }
    }

# %% Same YAML file
with open(args.output, 'w') as fh:
    yaml.dump(ci, fh)
