import yaml

import pyAMI_atlas.api as AtlasAPI

class Tag:
    """
    Describes the complete setup used to run a transform (aka a tag).

    Consists of the following properties:
    - release: the release Project,version (ie. `Athena,22.0.40`)
    - transform: the transform to run (ie: `Reco_tf`)
    - args: a dictionary of arguments to pass to the transform.

    The arguments are build from `args` as `--key value`. If `value` is a list,
    then the elements are added as `--key 'value1' 'value2' ...`.
    """
    def __init__(self,release,transform,args={}):
        self.release=release
        self.transform=transform
        self.args=args

    def buildargs(self):
        """
        Return the list of arguments that can then be passed to `subprocess`.

        Special processing:
         - If `$ATHENA_CORE_NUMBER` is found in the `athenaopts` argument, then
           it is replaced with the value `1`.
        """
        args=[]
        for k,v in self.args.items():
            if v is None: # Skip blank
                continue

            listv=type(v) is list

            # Special replacements
            if k=='athenaopts':
                if listv:
                    v=map(lambda v: v.replace('$ATHENA_CORE_NUMBER','1'), v)
                else:
                    v=v.replace('$ATHENA_CORE_NUMBER','1')

            # Add to list
            args.append(f'--{k}')
            if listv:
                args+=v
            else:
                args.append(v)

        return args

class TagBuilder:
    """
    Build tags for requests by querying AMI and merging results with custom
    definitions.

    A tag can be obtained from the `tag` function.
    """
    def __init__(self, tags, ami):
        """
        Parameters
        ----------
        request : dict
            dictionary containing custom tag definitions
        ami : pyAMI.client.Client
            AMI client to use for querying AMI.
        """
        # Store AMI client
        self.ami=ami

        self.tags={}
        for tname,tdata in tags.items():
            # Tag information that will be build in two steps:
            # 1. Get the tag from AMI (if `inherit` is present)
            # 2. Overwrite with any local definitions
            tag=None

            # Load data from AMI
            if 'inherit' in tdata:
                tag=self.amitag(tdata['inherit'])
            else:
                tag=Tag(None, None, {})

            # Override with local definitions
            tag.release  =tdata.get('release'  ,tag.release  )
            tag.transform=tdata.get('transform',tag.transform)
            tag.args.update(tdata.get('args',{}))

            # Save tag to local database
            self.tags[tname]=tag

    def amitag(self, tag):
        """
        Returns a `Tag` object by querying AMI. It does not merge with any
        custom definitions
        """
        ami,prod=AtlasAPI.get_ami_tag(self.ami, tag)

        trf=prod.pop('transformation')
        release=prod.pop('SWReleaseCache').replace('_',',')
        args={}

        # Pop keywords that are not arguments and save the rest
        special=['productionStep', 'tagType', 'tagNumber', 'groupName', 'cacheName', 'baseRelease', 'transformationName', 'description', 'created', 'createdBy', 'modified', 'modifiedBy', 'transformation', 'SWReleaseCache']
        for kw in special:
            prod.pop(kw, None)

        # Add remaining keys as args, making them ready for command line
        for k,v in prod.items():
            # Remove double quotation
            quote=None
            if v.startswith('"') and v.endswith('"'):
                quote='"'
            elif v.startswith("'") and v.endswith("'"):
                quote="'"

            if quote is not None:
                v=v.strip(quote) # Remove surrounding quotation marks
                v=v.split(f'{quote} {quote}') # multiargs
                v=list(map(lambda v: v.replace(f'\\{quote}',quote), v)) # replace escaped quotation
                if len(v)==1: # not a multiarg if only a single arg
                    v=v[0]

            args[k]=v

        # Build the object
        tobj=Tag(release, trf, args)
        return tobj

    def tag(self, tag):
        """
        Return the tag information for the given `tag` name.
        """
        # Locally defined
        if tag in self.tags: 
            return self.tags[tag]

        # Lookup in AMI
        tobj=self.amitag(tag)
        self.tags[tag]=tobj # cache

        return self.tags[tag]