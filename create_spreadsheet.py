#!/usr/bin/env python

# %% Important packages
import re
import argparse
from typing import OrderedDict

import importlib_resources

import pyexcel_ods3

from mcprodtester import workflow
from mcprodtester import tags
from mcprodtester import request

# %% Input arguments
parser = argparse.ArgumentParser(description='Create the request spreadsheet for a request.')
parser.add_argument('request' , help='Request file')

args = parser.parse_args()

# %% Open the request
request = request.load(args.request)
samples=request.get('samples',[])

# %% Helpful functions for parsing request settings
def get_tag_merge(tag, prefix):
    """
    Extracts main and merge tags from a tag in the form of
    `{prefix}###_{prefix}###`.

    Returns
    -------
    `(main, merge)` if the format is matched, `None` otherwise.
    """
    parts=tag.split('_')
    if len(parts)!=2:
        return None
    if not parts[0].startswith(prefix) and not parts[1].startswith(prefix):
        return None
    return parts[0],parts[1]

def scope_to_cem(scope):
    """
    Converts the scope string to the center of mass energy.

    Parameters
    ----------
    scope : str
        Scope in the format `"aaC_EEpETeV"`

    Returns
    -------
    The center of mass energy in TeV.
    """
    match=re.match('[a-z]+[^_]+_([0-9]+)(p([0-9]).*)?TeV', scope)
    if match is None:
        raise ValueError(f'Invalid format: {scope}')
    ecm=int(match.group(1))*1000
    if match.group(3)!=None:
        ecm+=int(match.group(3))*100
    return ecm
    

# %% Loop over samples and add to request
rows=[]
for sample in samples:
    row={}
    row['DSID'         ]=sample['dsid']
    row['E_CoM [GeV]'  ]=scope_to_cem(sample.get('scope','mc16_13p6TeV'))
    row['Priority'     ]=1
    row['Output events']=sample.get('events','')
    row['Comments'     ]=sample.get('comment','')
    for stage in request['workflows'][sample['workflow']]:
        if   stage['output']=='EVNT':
            etag=get_tag_merge(stage['tag'],'e')
            if etag is None:
                row['Evgen tag'] = stage['tag']
            else:
                row['Evgen tag']       = etag[0]
                row['Evgen merge tag'] = etag[1]
        elif stage['output']=='HITS':
            stag=get_tag_merge(stage['tag'],'s')
            if stag is None:
                row['Simul tag'] = stage['tag']
            else:
                row['Simul tag'] = stag[0]
                row['Merge tag'] = stag[1]
        elif stage['output']=='RDO':
            row['Digi tag'] = stage['tag']
        elif stage['output']=='AOD':
            rtag=get_tag_merge(stage['tag'],'r')
            if rtag is None:
                row['Reco tag'] = stage['tag']
            else:
                row['Reco tag'] = rtag[0]
                row['Rec Merge tag'] = rtag[1]

    rows.append(row)

# %% Format into pyexcel data
columns=['DSID', 'Event input for evgen (optional)', 'E_CoM [GeV]', 'Output events', 'Type (Evgen, FullSim, AF2, FCSv2, FastChain, ...)', 'Priority', 'Output formats', 'Evgen Release', 'Comments', 'Evgen tag', 'Evgen merge tag', 'Simul tag', 'Merge tag', 'Digi tag', 'Reco tag', 'Rec Merge tag', 'Deriv tag', 'Deriv merge tag', 'Rivet routines']
data=[columns]
for row in rows:
    data.append([row.get(column,'') for column in columns])

# %% Save to File
odsdata = OrderedDict()
odsdata.update({'Sheet 1':data})

pyexcel_ods3.save_data("request.ods", odsdata)