#!/bin/bash

if [ ${#} == 0 ]; then
    echo "usage: ${0} Project,release command"
    exit 1
fi
release=${1}

#
# Setup the environment
echo "Setup release ${release}"

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=${HOME}/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
source $AtlasSetup/scripts/asetup.sh ${release}

export ATHENA_CORE_NUMBER=1

#
# Run the transform
"${@:2}"
