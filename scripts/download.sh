#!/bin/bash

if [ ${#} == 0 ]; then
    echo "usage: ${0} dir dsid"
    exit 1
fi
dir=${1}
dsid=${@:2}

#
# Setup the environment
echo "Setup rucio"

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=${HOME}/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/localSetup.sh rucio
if [ ${?} != 0 ]; then
    echo "ERROR: Failed to setup rucio"
    exit 1
fi

#
# Run the transform
rucio download --dir ${dir} ${dsid}
